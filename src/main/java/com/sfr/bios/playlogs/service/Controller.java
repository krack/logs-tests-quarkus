package com.sfr.bios.playlogs.service;

import javax.ws.rs.Consumes;
import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

@Path("/")
@Consumes(MediaType.APPLICATION_JSON)
@Produces(MediaType.APPLICATION_JSON)
public class Controller {

    
    protected final Logger log = LoggerFactory.getLogger(Controller.class);



    @GET
    public Response log(){

            log.info("Je te dis que je log");
            return Response.ok("Je te dit que je log").build();

    }

}