package com.sfr.bios.playlogs.service;

import javax.inject.Singleton;
import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.Date;

import io.quarkiverse.loggingjson.JsonProvider;
import io.quarkiverse.loggingjson.JsonGenerator;
import org.jboss.logmanager.ExtLogRecord;

@Singleton
public class MyJsonProvider implements JsonProvider {

    @Override
    public void writeTo(JsonGenerator generator, ExtLogRecord event) throws IOException {
        Date now = new Date();
        SimpleDateFormat formater = new SimpleDateFormat("MM-dd-YYYY");
        generator.writeStringField("date_log", formater.format(now));
        
        SimpleDateFormat formater2 = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss.SSS");
        generator.writeStringField("date_log_ms", formater2.format(now)); 
    }
}